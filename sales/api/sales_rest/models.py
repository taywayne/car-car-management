from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=11)

    def get_api_url(self):
        return reverse("customers_list", kwargs={"pk": self.id})

class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("salespersons_list", kwargs={"employee_id": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name="automobile", on_delete=models.PROTECT,)
    salesperson = models.ForeignKey(Salesperson, related_name="salesperson", on_delete=models.PROTECT,)
    customer  = models.ForeignKey(Customer, related_name="customer", on_delete=models.PROTECT,)
    price = models.CharField(max_length=11)

    def get_api_url(self):
        return reverse("sales_list", kwargs={"pk": self.id})
