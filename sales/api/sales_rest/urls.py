from django.urls import path
from .views import (customers_list,
                    customer_view,
                    salesperson_list,
                    salesperson_view,
                    sales_list)

urlpatterns = [
    path("salespeople/", salesperson_list, name="salesperson_list"),
    path("salespeople/<int:pk>", salesperson_view, name="salesperson_view"),
    path("customers/", customers_list, name="customers_list"),
    path("customers/<int:pk>", customer_view, name="customer_view"),
    path("sales/", sales_list, name="sales_list"),
]
