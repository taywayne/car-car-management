import React, { useState, useEffect } from "react";

function ShowAuto() {
    const [autos, setAutos] = useState([])

    async function loadAuto() {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url)
        if (response.ok) {
        const data = await response.json()
        setAutos(data.autos)
        }
    }

    useEffect(() => {
        loadAuto();
    }, []);

    return (
        <div className="container">
            <h2>All Automobiles</h2>
            <div className='row'>
            {autos.map((auto) => (
                <div key={auto.id} className='col'>

                    <div className='card my-3 p-3 rounded'>

                        <div className='card-body'>
                            <p>
                                <strong>Model:</strong> {auto.model.name}
                            </p>
                            <p>
                                <strong>Color:</strong> {auto.color}
                            </p>
                            <p>
                                <strong>year:</strong> {auto.year}
                            </p>
                            <p>
                                <strong>vin:</strong> {auto.vin}
                            </p>
                            <p>
                                <strong>sold:</strong> {auto.sold ? "Yes" : "No"}
                            </p>

                        </div>

                    </div>

                </div>

            ))}
        </div>
        </div>
    );
}

export default ShowAuto;
