import React, { useState, useEffect } from "react"

function ListSalespeople() {
    const [salesperson, setSalespeople] = useState([])

    async function loadSalespeople() {
        const url = "http://localhost:8090/api/salespeople/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salesperson)
        }
    }

    useEffect(() => {
        loadSalespeople()
    }, []);

    return (
        <>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson.map((person) => (
                            <tr key={person.id}>
                                <td>{ person.employee_id }</td>
                                <td>{ person.first_name }</td>
                                <td>{ person.last_name }</td>
                            </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default ListSalespeople;
