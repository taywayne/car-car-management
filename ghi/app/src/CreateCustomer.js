import React, { useState } from 'react';

function CreateCustomer() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phone_number, setPhoneNumber] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        first_name,
        last_name,
        address,
        phone_number,
        };

        const url = 'http://localhost:8090/api/customers/';
        const newCustomer = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };

        const response = await fetch(url, newCustomer);
        if (response.ok) {
        const newCustomer = await response.json();
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');
        }
    }

    const handleFirstName = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastName = (event) => {
        setLastName(event.target.value);
    };

    const handleAddress = (event) => {
        setAddress(event.target.value);
    };

    const handlePhoneNumber = (event) => {
        setPhoneNumber(event.target.value);
    };

    return (
        <div className='row'>
        <div className='offset-3 col-6'>
            <div className='p-4 mt-4'>
            <h2>Create a new Customer</h2>
            <form onSubmit={handleSubmit}>
                <div className='form-floating mb-3'>
                <input type='text' id='first_name' value={first_name} onChange={handleFirstName} placeholder='first name' required name='first_name' className='form-control' />
                <label htmlFor='first_name'>First Name:</label>
                </div>

                <div className='form-floating mb-3'>
                <input type='text' id='last_name' value={last_name} onChange={handleLastName} placeholder='last name' required name='last_name' className='form-control' />
                <label htmlFor='last_name'>Last Name:</label>
                </div>

                <div className='form-floating mb-3'>
                <input type='text' id='address' value={address} onChange={handleAddress} placeholder='address' required name='address' className='form-control' />
                <label htmlFor='address'>Address:</label>
                </div>

                <div className='form-floating mb-3'>
                <input type='text' id='phone_number' value={phone_number} onChange={handlePhoneNumber} placeholder='phone number' required name='phone_number' className='form-control' />
                <label htmlFor='phone_number'>Phone Number:</label>
                </div>

                <button className='btn btn-primary'>Create Customer</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default CreateCustomer;
