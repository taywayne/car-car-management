import React, { useState, useEffect } from "react"

function ListCustomers() {
    const [customers, setCustomers] = useState([])

    async function loadCustomers() {
        const url = "http://localhost:8090/api/customers/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        loadCustomers()
    }, []);

    return (
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((person) => (
                            <tr key={person.id}>
                                <td>{ person.first_name }</td>
                                <td>{ person.last_name }</td>
                                <td>{ person.address }</td>
                                <td>{ person.phone_number }</td>
                            </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default ListCustomers;
