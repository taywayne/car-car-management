import React, { useState, useEffect } from "react"

function ListSales() {
    const [sale, setSale] = useState([])

    async function loadSales() {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setSale(data.sale)
        }
    }

    useEffect(() => {
        loadSales()
    }, []);

    return (
        <>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperon ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sale.map((ticket) => (
                            <tr key={ticket.id}>
                                <td>{ ticket.salesperson.employee_id }</td>
                                <td>{ ticket.salesperson.first_name } { ticket.salesperson.last_name }</td>
                                <td>{ ticket.customer.first_name } { ticket.customer.last_name }</td>
                                <td>{ ticket.automobile.vin }</td>
                                <td>{ ticket.price }</td>
                            </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default ListSales;
