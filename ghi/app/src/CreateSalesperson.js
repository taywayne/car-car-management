import React, { useState } from 'react';

function CreateSalesperson() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
        first_name,
        last_name,
        employee_id,
        };

        const url = 'http://localhost:8090/api/salespeople/';
        const newperson = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };


        const response = await fetch(url, newperson);
        if (response.ok) {
        const newSalesperson = await response.json();

        setFirstName('');
        setLastName('');
        setEmployeeId('');
        }
    }

    const handleFirstName = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastName = (event) => {
        setLastName(event.target.value);
    };

    const handleEmployeeId = (event) => {
        setEmployeeId(event.target.value);
    };

    return (
        <div className='row'>
        <div className='offset-3 col-6'>
            <div className='p-4 mt-4'>
            <h2>Create a new Salesperson</h2>
            <form onSubmit={handleSubmit}>
                <div className='form-floating mb-3'>
                <input type='text' id='first_name' value={first_name} onChange={handleFirstName} placeholder='first name' required name='first_name' className='form-control' />
                <label htmlFor='first_name'>First Name:</label>
                </div>

                <div className='form-floating mb-3'>
                <input type='text' id='last_name' value={last_name} onChange={handleLastName} placeholder='last name' required name='last_name' className='form-control' />
                <label htmlFor='last_name'>Last Name:</label>
                </div>

                <div className='form-floating mb-3'>
                <input type='text' id='employee_id' value={employee_id} onChange={handleEmployeeId} placeholder='employee id' required name='employee_id' className='form-control' />
                <label htmlFor='employee_id'>Employee ID:</label>
                </div>

                <button className='btn btn-primary'>Create Salesperson</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default CreateSalesperson;
