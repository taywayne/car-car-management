import React, { useEffect, useState } from 'react'

function CreateSale() {
    const [price, setPrice] = useState('')
    const [automobile, setAutomobile] = useState('')
    const [autos, setAutomobiles] = useState([])
    const [person, setSalesperson] = useState('')
    const [salesperson, setsSalespersons] = useState([])
    const [customer, setCustomer] = useState('')
    const [customers, setCustomers] = useState([])

// grab all automobiles
    useEffect(() => {
        async function getAutomobiles() {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)


        }
        }
        getAutomobiles()
        }, []);

// get all salespeople
    useEffect(() => {
        async function getSalespersons() {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setsSalespersons(data.salesperson)
        }
        }
        getSalespersons()
        }, []);

// get all customers
    useEffect(() => {
        async function getCustomers() {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)

        }
        }
        getCustomers()
        }, []);



    async function handleSubmit(event) {
        event.preventDefault()
        const data = {
        'automobile': automobile,
        'salesperson': person,
        'customer': customer,
        price,
        }

        const urlSale = 'http://localhost:8090/api/sales/'
        const options = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        }


        const response = await fetch(urlSale, options)

            if (response.ok) {

                const newSale = await response.json()

                setAutomobile('')
                setSalesperson('')
                setCustomer('')
                setPrice('')

                }
        }

        const handleAutomobile = (event) => {
            setAutomobile(event.target.value);
        };

        const handleSalesperson = (event) => {
            setSalesperson(event.target.value);
        };

        const handleCustomer = (event) => {
            setCustomer(event.target.value);
        };

        const handlePrice = (event) => {
            setPrice(event.target.value);
        };




    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='p-4 mt-4'>
                    <h2>Create a new sale</h2>
                    <form onSubmit={handleSubmit}>



                        <div className="form-floating mb-3">

                            <select id="automobile" value={automobile} onChange={handleAutomobile}  multiple={false} name='automobile' className="form-control">
                            <option value="">Choose a Automobile</option>

                            {autos.filter((auto) => !auto.sold).map((automobile) => (
                                <option key={automobile.vin} value={automobile.vin}>
                                {automobile.vin}
                                </option>
                            ))}

                            </select>
                            <label htmlFor="model">Automobile:</label>
                        </div>



                        <div className="form-floating mb-3">

                        <select id="customer" value={customer} onChange={handleCustomer}  multiple={false} name='customer' className="form-control">
                        <option value="">Choose a Customer</option>

                        {customers.map((customer) => (
                            <option key={customer.id} value={customer.id}>
                            {customer.first_name} {customer.last_name}
                            </option>
                        ))}

                        </select>
                        <label htmlFor="model">Customer:</label>
                        </div>



                        <div className="form-floating mb-3">

                            <select id="person" value={person} onChange={handleSalesperson}  multiple={false} name='person' className="form-control">
                            <option value="">Choose a Salesperson</option>

                            {salesperson.map((person) => (
                                <option key={person.id} value={person.id}>
                                {person.first_name} {person.last_name}
                                </option>
                            ))}

                            </select>
                            <label htmlFor="model">Salesperson:</label>
                        </div>



                        <div className="form-floating mb-3">
                        <input type="text" id="price" value={price} onChange={handlePrice} placeholder="price" name="price" className="form-control"/>
                        <label htmlFor="color">Price:</label>
                        </div>

                        <button className="btn btn-primary">Create Sale</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CreateSale
